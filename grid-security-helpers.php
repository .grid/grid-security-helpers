<?php
/*
 * Plugin Name: GRID Security Helpers
 * Description: Helpers functions & utilities to improve WordPress Security through a MU Plugin
 * Version: 0.4
 * Author: GRID Agency
 * Author URI: https://www.grid-agency.com
*/

if( !defined('ABSPATH') ) {
	die();
}

/**
 * Block External requests
 * Within the backoffice, WP makes several external calls to check for updates, news RSS etc.
 * Depending on internal Policies, we can disable them to avoid a waiting time due to Timeout, and specifically allow one or more of those hosts.
 */
//define('WP_HTTP_BLOCK_EXTERNAL', true);
//define('WP_ACCESSIBLE_HOSTS', 'site1.com, site2.com');

/**
 * Disable File editing
 * Remove WordPress feature that allow online edition of themes & plugins files from the backoffice
 */
define('DISALLOW_FILE_EDIT', true);

/**
 * Hide Login hints
 * When a login attempt fails, WP shows an error message which confirms the exitence of an user ID.
 * Instead, we should show a 'standard' error message for all cases.
 */
function grid_mu_hide_login_hints(){
	return __('Wrong credentials', 'grid-security-helpers');
}
add_filter( 'login_errors', 'grid_mu_hide_login_hints' );

/**
  * Hide WordPress version
  *
  * WordPress adds a custom META to display the current version.
  * It is also displayed within RSS feeds, & as assets suffix (CSS, JS)
  */

  // Remove META
  remove_action('wp_head', 'wp_generator');

  // Remove from RSS feeds
  function grid_mu_hide_wp_version(){
    return false;
  }
  add_filter('the_generator', 'grid_mu_hide_wp_version');

  // Remove Assets suffixes
  function grid_mu_remove_assets_suffixes( $src ){
      if( strpos($src, 'ver=') ){
          $src = remove_query_arg('ver', $src);
      }
      return $src;
  }
  add_filter('style_loader_src', 'grid_mu_remove_assets_suffixes', 9999);
  add_filter('script_loader_src', 'grid_mu_remove_assets_suffixes', 9999);


/**
  * Deactivate REST API
  * For security reason, calls to WP REST API should be made from logged in users.
  * By default, all routes are public and can expose user accounts: https://website.com/wp-json/wp/v2/users
  */
function grid_mu_restrict_rest_api( $results ){
	if( !empty( $results ) ){
		return $results;
	}

	if( !is_user_logged_in() ){
		return new WP_Error('rest_not_logged_in', 'You are not logged in.', array( 'status' => 401));
	}

	return $results;
}
add_filter('rest_authentication_errors', 'grid_mu_restrict_rest_api');

/**
 * Deactivate XML-RPC
 * XML-RPC is an old protocol to publish content from POST queries, or get pingbacks
 */

// Disable XML-RPC methods
add_filter('xmlrpc_enabled', '__return_false');

// Hide all apis listed in xmlrpc.php file - a little extreme :)
if( defined( 'XMLRPC_REQUEST') && XMLRPC_REQUEST ) exit;

// Hide XML-RPC Meta shown in <head>
remove_action('wp_head', 'rsd_link');

// Add XML-RPC custom directive on .htaccess
function grid_mu_apache_xmlrpc( $rules ){
$custom_apache_rules = <<<EOD
	\n # Disallow requests to xmlrpc.php
	<Files xmlrpc.php>
		Order Allow,Deny
		Deny from all
	</Files>
	#end \n
EOD;

    return $rules . $custom_apache_rules;
}
add_filter('mod_rewrite_rules', 'grid_mu_apache_xmlrpc');

/**
 * Protect sensitive WP files
 * We should make sure that .htaccess, .htpasswd, wp-config.php and other sensitive files are not accessible from anywhere
 */
function grid_mu_apache_sensitive_files( $rules ){
    $custom_apache_rules = <<<EOD
    \n # Protect sensitive files (wp-config, php.ini, .htaccess & .htpasswd
    <FilesMatch "^.*(wp-config\.php|php.ini|\.[hH][tT][aApP].*)$">
        Order Allow,Deny
        Deny from all
    </FilesMatch>
    #end \n
EOD;

    return $rules . $custom_apache_rules;
}
add_filter('mod_rewrite_rules', 'grid_mu_apache_sensitive_files');

/**
 * Block PHP execution from Uploads directory
 * We should make sure PHP files can't be easily executed from the outside...
 */
function grid_mu_apache_php_files( $rules ){
    $path = ABSPATH;
    $path = $path . "wp-content/uploads";
    $custom_apache_rules = <<<EOD
    \n # Block PHP files execution
    <Directory "$path/**/*.php">
    #<Files "*.php">
        Order Allow,Deny
        Deny from all
    #</Files>
    </Directory>
    #end \n
EOD;

    return $rules . $custom_apache_rules;
}
//add_filter('mod_rewrite_rules', 'grid_mu_apache_php_files');